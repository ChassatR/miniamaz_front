import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location } from '@angular/common';
import { User } from '../_models';
import { UserService } from '../_services';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
    @Input() user: User;

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private location: Location
    ) {}

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.userService.getById(id).subscribe(user => this.user = user);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.userService.update(id, this.user).subscribe(() => this.goBack());
    }

}
