import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { UsersListComponent } from './users-list/users-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAddComponent } from './user-add/user-add.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { InventoryAddComponent } from './inventory-add/inventory-add.component';
import {InventoryEditComponent } from './inventory-edit/inventory-edit.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user_list', component: UsersListComponent },
    { path: 'user_edit/:id', component: UserEditComponent },
    { path: 'user_add', component: UserAddComponent},
    { path: 'product_list', component: ProductListComponent },
    { path: 'product_edit/:id', component: ProductEditComponent },
    { path: 'product_add', component: ProductAddComponent },
    { path: 'inventory_add', component: InventoryAddComponent },
    { path: 'inventory_edit/:id', component: InventoryEditComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
