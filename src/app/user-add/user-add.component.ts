import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Location } from '@angular/common';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthUserService } from '../_services';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private authUserService : AuthUserService,
        private alertService: AlertService,
        private location: Location) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            lastname: ['', Validators.required],
            firstname: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    goBack(): void {
        this.location.back();
    }

    onRegister() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.authUserService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    //this.alertService.success('Registration successful');
                    this.goBack();
                },
                error => {
                    //this.alertService.error(error);
                    this.loading = false;
                });
    }
}
