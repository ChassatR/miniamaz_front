import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Product } from '../_models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(`${environment.apiUrl}/api/products`, {
        headers : {
            'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        }
    });
  }
  getById(id: number) {
        return this.http.get<Product>(`${environment.apiUrl}/api/products/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    createProduct(product: Product) {
        return this.http.post<Product>(`${environment.apiUrl}/api/product/create`, product, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    update(id: number, product: Product) {
        return this.http.put(`${environment.apiUrl}/api/products/` + id, product, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/products/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }
}
