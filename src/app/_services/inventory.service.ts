import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Inventory } from '../_models';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(`${environment.apiUrl}/api/inventories`, {
        headers : {
            'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        }
    });
  }
  getById(id: number) {
        return this.http.get<Inventory>(`${environment.apiUrl}/api/inventories/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    createInventory(inventory: Inventory) {
        return this.http.post<Inventory>(`${environment.apiUrl}/api/inventory/create`, inventory, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    update(id: number, inventory: Inventory) {
        return this.http.put(`${environment.apiUrl}/api/inventories/` + id, inventory, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/inventories/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }
}
