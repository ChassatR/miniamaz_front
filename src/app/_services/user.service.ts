import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get(`${environment.apiUrl}/api/users`, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    getById(id: number) {
        return this.http.get<User>(`${environment.apiUrl}/api/users/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    update(id: number, user: User) {
        return this.http.put(`${environment.apiUrl}/api/users/` + id, user, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/users/` + id, {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        });
    }
}
