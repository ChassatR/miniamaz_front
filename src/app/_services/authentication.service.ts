import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

const httpOptions = {
 headers: new HttpHeaders({ 'Authorization': 'Basic ' + btoa('miniamaz_client_id:miniamaz_client_secret') })
};

//bearer localStorage.getItem("access_token") --> Basic ...

@Injectable()
export class AuthenticationService {
    test: string;



    constructor(private http: HttpClient) { }


    login(email: string, password: string) {
        let form = new FormData();

        form.append("username", email);
        form.append("password", password);
        form.append("grant_type", "password");

        return this.http.post<any>(`${environment.apiUrl}/oauth/token`, form, httpOptions);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
