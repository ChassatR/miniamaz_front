import { TestBed } from '@angular/core/testing';

import { LogoutUserService } from './logout-user.service';

describe('LogoutUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogoutUserService = TestBed.get(LogoutUserService);
    expect(service).toBeTruthy();
  });
});
