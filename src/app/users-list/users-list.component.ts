import { Component, OnInit } from '@angular/core';
import {UserService} from '../_services';
import { first } from 'rxjs/operators';
import {User} from '../_models';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
    users: any = [];

    constructor(private userService: UserService) {
 }

  ngOnInit() {
      this.loadAllUsers();
  }

  private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => this.loadAllUsers());
    }

}
