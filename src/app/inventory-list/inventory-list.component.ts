import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { InventoryService } from '../_services/inventory.service';


@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.css']
})
export class InventoryListComponent implements OnInit {
inventories: any = [];

  constructor(private inventoryService: InventoryService) { }

  ngOnInit() {
      this.loadAllInventories();
  }

  loadAllInventories() {
      this.inventoryService.getAll().pipe(first()).subscribe(inventories => {
            this.inventories = inventories;
        });
  }

  deleteProduct(id: number) {
     this.inventoryService.delete(id).subscribe(() => this.loadAllInventories());
  }

}
