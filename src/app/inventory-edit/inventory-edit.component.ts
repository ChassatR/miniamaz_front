import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location } from '@angular/common';
import { Inventory } from '../_models';
import { InventoryService } from '../_services';

@Component({
  selector: 'app-inventory-edit',
  templateUrl: './inventory-edit.component.html',
  styleUrls: ['./inventory-edit.component.css']
})
export class InventoryEditComponent implements OnInit {
@Input() inventory: Inventory;

    constructor(
        private route: ActivatedRoute,
        private inventoryService: InventoryService,
        private location: Location
    ) {}

    ngOnInit() {
        this.getProduct();
    }

    getProduct() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.inventoryService.getById(id).subscribe(inventory => this.inventory = inventory);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.inventoryService.update(id, this.inventory).subscribe(() => this.goBack());
    }

}
