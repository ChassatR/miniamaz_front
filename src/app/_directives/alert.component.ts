import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Alert, AlertType } from '../_models/alert';
import { AlertService } from '../_services/index';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit {
    alert: Alert;


    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getAlert(this.alert).subscribe((alert: Alert) => {
            if (!alert.message) {
                // clear alert when an empty alert is received
                this.alert = null;
                return;
            }

            // add alert to array
            this.alert = (alert);
        });
    }

    removeAlert() {
        this.alert = null;
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}
