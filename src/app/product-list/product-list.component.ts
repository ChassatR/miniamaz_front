import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ProductService } from '../_services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
    products: any = [];

  constructor(private productService: ProductService) { }

  ngOnInit() {
      this.loadAllProducts();
  }

  loadAllProducts() {
      this.productService.getAll().pipe(first()).subscribe(products => {
            this.products = products;
        });
  }

  deleteProduct(id: number) {
     this.productService.delete(id).subscribe(() => this.loadAllProducts());
  }

}
