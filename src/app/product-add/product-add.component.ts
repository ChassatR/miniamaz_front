import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Location } from '@angular/common';
import { first } from 'rxjs/operators';

import { AlertService, ProductService } from '../_services';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private productService: ProductService,
        private alertService: AlertService,
        private location: Location) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            label: ['', Validators.required],
            price: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    goBack(): void {
        this.location.back();
    }

    onRegister() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.productService.createProduct(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    //this.alertService.success('Registration successful');
                    this.goBack();
                },
                error => {
                    //this.alertService.error(error);
                    this.loading = false;
                });
    }
}
