import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location } from '@angular/common';
import { Product } from '../_models';
import { ProductService } from '../_services';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
@Input() product: Product;

    constructor(
        private route: ActivatedRoute,
        private productService: ProductService,
        private location: Location
    ) {}

    ngOnInit() {
        this.getProduct();
    }

    getProduct() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.productService.getById(id).subscribe(product => this.product = product);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.productService.update(id, this.product).subscribe(() => this.goBack());
    }

}
