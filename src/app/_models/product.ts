export class Product {
    id: number;
    label: string;
    price: number;
}
