export class User {
    userId: number;
    lastname: string;
    firstname: string;
    email: string;
    password: string;
}
