export class Inventory {
    id: number;
    label: string;
    creationDate: Date;
    closureDate: Date;
    quantities: Map<number, number>;
}
